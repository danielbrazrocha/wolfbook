import axios from "axios";

const api = axios.create({
  baseURL: "https://wolfbook.herokuapp.com/",
});

const user = JSON.parse(localStorage.getItem('user'));


/**
 * Função /validate_user para verificação do status
 * do usuário
 */
async function validate_user() {
  const axiosConfig = {
    headers: {
      "Content-Type": "application/json",
      Authorization:
        "eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoyfQ.HKIe-5JyBakeZLUlCWxhBiEtQhNPMOum0aBghR1PLl0",
    },
  };

  try {
    const resData = await api.get("/validate_user", axiosConfig);
    const res = resData.data;
    //console.log(res);
    return res;
  } catch (error) {
    console.log(error)
  }
}


/** 
 * Função para criar conta
 */
async function sign_up(data) {
    const axiosConfig = {
      headers: {
        "Content-Type": "application/json",      
      },
    };
    try {
      const resData = await api.post("/sign_up",
      {
        "user":{
        "name": data.get('name'),
        "email": data.get('email'),
        "gender": data.get('gender'),
        "password": data.get('password'),
        "password_confirmation": data.get('password_confirmation'),
        "birthdate": data.get('birthdate') }
    },
      axiosConfig);
      console.log(resData.request._header);
      console.log(resData);
      const res = resData.data;
      return res;
    } catch (error) {
      console.log(error)
    }
  }


/** 
 * Função para efetuar login conta
 */
async function login(data) {
  const axiosConfig = {
    headers: {
      "Content-Type": "application/json",      
    },
  };
  try {
    const resData = await api.post("/login",
    { "user":{
      "email": data.get('loginEmail'),
      "password": data.get('loginPassword'),}}
    , axiosConfig);
    //console.log(resData.request._header);
    //console.log(resData);
    const res = resData.data;
    console.log(res.token);
    if (res) {
      localStorage.setItem("user", JSON.stringify(res));
    }
    return res;
  } catch (error) {
    console.log(error)
  }
}

/** 
 * Função para buscar todos os posts do site
 */
async function posts(data) {
  const axiosConfig = {
    headers: {
      "Content-Type": "application/json",   
      "Authorization": user.token
    },
  };
  try {
    const resData = await api.get("/posts", axiosConfig);
    //console.log(resData.request._header);
    const res = resData.data;
    //console.log(res.Data)
    //console.log(res)
    return res;
  } catch (error) {
    console.log(error)
  }
}

/** 
 * Função para buscar todos os lobos da alcateia.
 */
async function friendships(data) {
  const axiosConfig = {
    headers: {
      "Content-Type": "application/json",   
      "Authorization": user.token
    },
  };
  try {
    const resData = await api.get("/friendships", axiosConfig);
    //console.log(resData.request._header);
    const res = resData.data;
    //console.log(res.Data)
    //console.log(res)
    return res;
  } catch (error) {
    console.log(error)
  }
}

async function mywolfs(data) {
  const axiosConfig = {
    headers: {
      "Content-Type": "application/json",   
      "Authorization": user.token
    },
  };
  try {
    const resData = await api.get("/users", axiosConfig);
    //console.log(resData.request._header);
    const res = resData.data;
    //console.log(res.Data)
    //console.log(res)
    return res;
  } catch (error) {
    console.log(error)
  }
}





export { 
  validate_user,
  login,
  posts,
  friendships,
  mywolfs,
  sign_up
 };
