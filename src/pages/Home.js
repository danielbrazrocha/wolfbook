import React, { useEffect } from "react";
import NavbarTop from "../components/NavBar/NavBar";
import Footer from "../components/Footer/Footer";
import Login from "../components/Login/Login";
import * as api from '../services/apiService';

const user = JSON.parse(localStorage.getItem("user"));

export default function Home(props) {

  
  useEffect(() => {
    const getValidateTest = async () => { await api.validate_user(); 
      };
      getValidateTest();
    }, []);



  return (
    <div className="default-flex-column">
      {user && <NavbarTop />}
      <Login />
      <Footer />
    </div>
  );
}
