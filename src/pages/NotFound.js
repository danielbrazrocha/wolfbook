import React from "react";
import Footer from "../components/Footer/Footer";
import logo from '../img/logo_fullsize.png';
import { Container } from "react-bootstrap";

export default function NotFound() {
  return (
    <Container id="notFound" Fluid>
      <img
        id="logo_img_full"
        alt="wolfbook logo"
        width="200px"
        src={logo}
        className="d-flex m-auto justify-center align-center"
      />
      <h1 className="m-auto text-center">Você está perdido da sua matilha.</h1>
      <h3 className="m-auto text-center ">404 - Page not found!</h3>
      <Footer />
    </Container>
  );
}
