import React, { useState, useEffect } from 'react'
import Navbar from '../components/NavBar/NavBar'
import Footer from '../components/Footer/Footer'
import * as api from '../services/apiService';
import Wolfs from '../components/Wolfs/Wolfs';

export default function AddWolf() {
    const [allWolfs, setAllWolfs] = useState([]);
    
    useEffect( () => {
        //Chamando API no Effect para carregar os posts logo que a páginas
        //é carregada
        const getAllWolfs = async () => {
            const allWolfsData = await api.mywolfs();    
            console.log(allWolfsData)

          console.log("Effect - Load")
          setTimeout(() => {
            setAllWolfs(allWolfsData);
          }, 1000);
        };
        getAllWolfs();
      }, []);

    return (
        <div>
            <Navbar />
            <Wolfs allWolfs={allWolfs}/>
            <Footer />
        </div>
    )
}
