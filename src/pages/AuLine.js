import React, { useState, useEffect } from 'react'
import { Spinner } from 'react-bootstrap'
import Footer from '../components/Footer/Footer'
import Navbar from '../components/NavBar/NavBar'
import * as api from '../services/apiService';
import Cards from '../components/Cards/Cards';

export default function AuLine() {
    const [allPosts, setAllPosts] = useState([]);
    
    useEffect( () => {
        //Chamando API no Effect para carregar os posts logo que a páginas
        //é carregada
        const getAllPosts = async () => {
            const allPostsData = await api.posts();    
            console.log(allPostsData)

          console.log("Effect - Load")
          setTimeout(() => {
            setAllPosts(allPostsData);
          }, 1000);
        };
        getAllPosts();
      }, []);
    

    return (
        <div>
            <Navbar />
            {allPosts ? <Cards allPosts={allPosts} /> : <Spinner id="spinner" animation="border" variant="dark" />}
            <Footer />
        </div>
    )
}
