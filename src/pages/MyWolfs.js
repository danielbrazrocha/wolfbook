import React, { useState, useEffect } from 'react'
import Navbar from '../components/NavBar/NavBar'
import Footer from '../components/Footer/Footer'
import { Spinner } from 'react-bootstrap'
import * as api from '../services/apiService';
import Friends from '../components/Friends/Friends';

export default function MyWolfs() {
    const [allFriends, setAllFriends] = useState([]);
    
    useEffect( () => {
        //Chamando API no Effect para carregar os posts logo que a páginas
        //é carregada
        const getAllWolfs = async () => {
            const allWolfsData = await api.friendships();    
            console.log(allWolfsData)

          console.log("Effect - Load")
          setTimeout(() => {
            setAllFriends(allWolfsData);
          }, 1000);
        };
        getAllWolfs();
      }, []);
    console.log(allFriends)
    return (
        <div id="notFound" className="m-auto text-center">
            <Navbar />
            {allFriends.length == 0 ? <h1> Sua alcateia ainda não foi formada.</h1> : <Friends allFriends={allFriends} />}
            <Footer />
        </div>
    )
}
