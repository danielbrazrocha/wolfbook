import React from "react";
import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faFacebookSquare } from "@fortawesome/free-brands-svg-icons";
import { faInstagramSquare } from "@fortawesome/free-brands-svg-icons";
import './Footer.css'

export default function Footer() {
  return (
    <Nav>
      <Nav fixed="bottom" className="bg-dark" id="FooterBarBg">
        <Navbar id="testBar" className="bg-dark" variant="dark" fixed="bottom">
          <Nav id="FooterBarItems" className="" >
            <Nav className="">
              <a href="https://www.facebook.com/injunioruff" target="_blank" rel="noopener noreferrer">
              <FontAwesomeIcon className="m-2" icon={faFacebookSquare} size="2x"  />
              </a>
              <a href="https://www.instagram.com/injunioruff/" target="_blank" rel="noopener noreferrer">
              <FontAwesomeIcon className="m-2" icon={faInstagramSquare} size="2x" />
              </a>
            </Nav>
            <Nav>
              <Navbar.Brand className="" href="http://www.injunior.com.br" target="_blank" rel="noopener noreferrer">
                <img
                  id="footer_logo"
                  alt=""
                  src="https://injunior.com.br/wp-content/uploads/2018/11/logo-branca.png"
                  width="30"
                  height="30"
                  className=""
                />{" "}
              </Navbar.Brand>
            </Nav>
          </Nav>
        </Navbar>
      </Nav>
    </Nav>
  );
}
