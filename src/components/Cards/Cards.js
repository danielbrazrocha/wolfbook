import React from "react";
import "./Cards.css";
import Card from "../Card/Card";

export default function Cards({allPosts}) {
   
  return (
    <div className="cards">
      <ul>
        {allPosts.map((item) => {
          return (
            <Card key={item.id} msg={item} />
          );
        })}
      </ul>
    </div>
  );
}
