import React from "react";
import "./Login.css";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import Image from "react-bootstrap/Image";
import logo_text from "../../img/logo_text.png";
import * as api from '../../services/apiService';

export default function Login() {




  const handleLoginSubmit = (event) => {
    /**
     * Subfunção para chamar a API e retornar um alerta sobre a autenticação
     * ou não do usuário
     */
    const submitLoginForm = async (form) => {
      //Chamando API passando as informações para a requisição
      const validateForm = await api.login(form);    
      // IF somente para retornar o sucesso ou não ao usuário
      if (validateForm) {
        //window.alert("Seu usuário foi logado com sucesso")
        window.location.reload();
      }
      else {
        window.alert("Ocorreu um erro no seu login. Verifique os dados e tente novamente")
      }
      console.log("fim");
      };
    
    /**
     * Função principal
     */
    //Chamado para evitar recarregamento do formulário      
    event.preventDefault();
    //Utilizando a classe FormData para pegar as informações dos inputs
    const data = new FormData(event.target);
    //Chamando a subfunção e passando os parâmetros formatados
    submitLoginForm(data);
  ;}
    


  const handleNewUserSubmit = (event) => {
    /**
     * Subfunção para chamar a API e retornar um alerta sobre a criação
     * ou não do usuário
     */
    const submitForm = async (form) => {
      //Chamando API passando as informações para a requisição
      const validateForm = await api.sign_up(form);    
      // IF somente para retornar o sucesso ou não ao usuário
      if (validateForm) {
        window.alert("Seu usuário foi criado com sucesso")
      }
      else {
        window.alert("Ocorreu um erro na sua requisição. Verifique os dados e tente novamente")
      }
      console.log("fim");
      };


    /**
     * Função principal
     */
    //Chamado para evitar recarregamento do formulário      
    event.preventDefault();
    //Utilizando a classe FormData para pegar as informações dos inputs
    const data = new FormData(event.target);
    //Convertendo a data do formato americano (MM-DD-AAA) para o 
    //exigido pela API (DD/MM/AAAA)
    const [year, month, day] = data.get('birthdate').split('-');
    const birthdateBr = `${day}/${month}/${year}`;
    //Gravando as alterações no FormData
    data.set('birthdate', birthdateBr);
    //Chamando a subfunção e passando os parâmetros formatados
    submitForm(data);
  };

  return (
    <div id="first_panel">
      <div id="panel_left">

        <Image src={logo_text} fluid />
        <p>
          <span id="front_text">A rede socialcateia.</span>
        </p>
        
        <Form onSubmit={handleNewUserSubmit}>
          <Form.Group  controlId="name">
            <Form.Control name="name" type="text" placeholder="Nome" />
            <Form.Text className="text-muted"></Form.Text>
          </Form.Group>

          <Form.Group controlId="email">
            <Form.Control name="email" type="email" placeholder="Email" />
            <Form.Text className="text-muted"></Form.Text>
          </Form.Group>

          <Form.Group controlId="password">
            <Form.Control name="password" type="password" placeholder="Senha"/>
          </Form.Group>

          <Form.Group controlId="password_confirmation">
            <Form.Control name="password_confirmation" type="password" placeholder="Confirme sua senha"/>
          </Form.Group>

          <Form.Group id="gender" controlId="gender">
            <input type="radio" id="male" name="gender" value="M"/>Masculino
            <input type="radio" id="female" name="gender" value="F" />Feminino
          </Form.Group>

          <Form.Group controlId="birthdate">
            <Form.Control name="birthdate" type="date" placeholder="Data de Nascimento"/>
          </Form.Group>

          <div id="signup">
            <Button variant="success" type="submit">
              Crie sua conta
            </Button>
            <div id="borderline"></div>
          </div>
        </Form>
      </div>

      <div id="panel_right">
        <div id="subpanel_login">
          <Form onSubmit={handleLoginSubmit}>
            <Form.Group controlId="formBasicLoginEmail">
              <Form.Control name="loginEmail" type="email" placeholder="Email" />
              <Form.Text className="text-muted"></Form.Text>
            </Form.Group>

            <Form.Group controlId="formBasicLoginPassword">
              <Form.Control name="loginPassword" type="password" placeholder="Senha" />
            </Form.Group>
            <Button variant="primary" type="submit">
              Entrar
            </Button>
            
          </Form>
        </div>
      </div>

      
    </div>
  );
}
