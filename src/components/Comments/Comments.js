import React from 'react'
import Comment from '../Comment/Comment';
import "./Comments.css";

export default function Comments({comments}) {
    console.log(comments)
    return (
        <div className="comments">
      <ul>
        {comments.comments.map((comment) => {
          return (
            <Comment key={comment.id} msg={comment} />
          );
        })}
      </ul>
    </div>
    )
}
