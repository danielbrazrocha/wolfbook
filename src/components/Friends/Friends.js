import React from "react";
import "./Friends.css";
import Friend from "../Friend/Friend";

export default function Cards({allPosts}) {
   
  return (
    <div className="cards">
      <ul>
        {allPosts.map((item) => {
          return (
            <Friend key={item.id} msg={item} />
          );
        })}
      </ul>
    </div>
  );
}
