import React from "react";
import Navbar from "react-bootstrap/Navbar";
import Button from "react-bootstrap/Button";
import Nav from "react-bootstrap/Nav";
import logo_text from '../../img/logo_text_white.png';
import logo from '../../img/logo_white.png';
import "./NavBar.css";
import { Link } from "react-router-dom";


export default function NavBar({orderByType}) {
  const logout = () => {
    localStorage.removeItem("user");
    window.location.reload();
  };


  return (
    <Nav id="NavBarBg" fixed="top" >
      <Navbar id="testBar" fixed="bottom">
        <Navbar
          id="subNavBar"
          collapseOnSelect
          expand="lg"
          fixed="top"
        >
          <Nav id="NavBarTop">
            <Navbar.Brand href="#home">
              <img
                id="logo_img"
                alt="wolfbook logo"
                src={logo}
                width="30"
                height="30"
                className="d-inline-block align-top"
              />
            </Navbar.Brand>
            <Navbar.Brand href="#home">
              <img
                id="logo_txt"
                alt="wolfbook text logo"
                src={logo_text}
                className="d-inline-block align-top"
              />
            </Navbar.Brand>
            <Navbar.Toggle aria-controls="responsive-navbar-nav" />
            <Link to="/auline" id="login_btn" className="nav-link">AuLine</Link>
            <Link to="/addwolf" id="login_btn" className="nav-link">Adicionar Lobos</Link>
            <Link to="/mywolfs" id="login_btn" className="nav-link">Sua Alcateia</Link>
            <Button onClick={logout} variant="dark">Sair</Button>{' '}
          </Nav>
        </Navbar>
      </Navbar>
    </Nav>
  );
}
