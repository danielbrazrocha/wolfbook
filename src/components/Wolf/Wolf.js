import React from "react";
//import { calcHoursDiff } from "../src/helpers/DateFormat";
import "./Wolf.css";
import profile from "../../img/profile.png";

export default function Wolf({ msg }) {
  return (
    <div className="wolf_card">
      <li key={msg.id}>
        <div id="wolf_card_content">
          <div id="wolf_card_content_left">
            <div>
              <img src={profile} alt="Profile pic"></img>
            </div>
            <div id="wolf_card_footer">
              <p><span id="author_name">{msg.name}</span></p>
            </div>
          </div>

          <div id="wolf_card_content_right">
            <div id="wolf_card_header">
              <div id="wolf_card_msg">
                <p>
                  <span id="author_name">{msg.name}</span>
                </p>
              </div>

            </div>
          </div>
        </div>
      </li>
    </div>
  );
}
