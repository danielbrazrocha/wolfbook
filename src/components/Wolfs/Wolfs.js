import React from "react";
import "./Wolfs.css";
import Wolf from "../Wolf/Wolf";

export default function Wolfs({allWolfs}) {
   console.log(allWolfs);
  return (
    <div className="cards">
      <ul>
        {allWolfs.map((item) => {
          return (
            <Wolf key={item.id} msg={item} />
          );
        })}
      </ul>
    </div>
  );
}
