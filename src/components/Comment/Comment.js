import React from 'react'
import "./Comment.css";

export default function Comment({msg}) {
    return (
        <div className="comment padding">
      <li key={msg.id}>
        <div id="comment_content">
            <div>
              <p><span> {msg.content}</span></p>
            </div>            
          </div>
      </li>
    </div>
    )
}
