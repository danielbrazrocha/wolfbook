




const calcHoursDiff = (param) => {
    const actualDate = new Date();
    const oldDate = new Date(param);
    const diff = Math.ceil(Math.abs( ((actualDate.getTime() - oldDate.getTime())/(1000 * 60 * 60)) ));
    return diff
  };


  
  export { calcHoursDiff };
