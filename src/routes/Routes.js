import React from "react";
import { Route, Switch, Redirect } from "react-router-dom";
import Home from "../pages/Home";
import AuLine from "../pages/AuLine";
import AddWolf from "../pages/AddWolf";
import MyWolfs from "../pages/MyWolfs";
import NotFound from "../pages/NotFound";

const user = JSON.parse(localStorage.getItem("user"));

export default function Routes() {
  return (
    <Switch>
      <Route exact path="/">
        {user ? <Redirect to="/AuLine" /> : <Home />}
      </Route>
      <Route exact path="/auline">
        {!user ? <Redirect to="/" /> : <AuLine />}
      </Route>
      <Route exact path="/addwolf">
        {!user ? <Redirect to="/" /> : <AddWolf />}
      </Route>
      <Route exact path="/mywolfs">
        {!user ? <Redirect to="/" /> : <MyWolfs />}
      </Route>
      <Route>
        <NotFound />
      </Route>
    </Switch>
  );
}
